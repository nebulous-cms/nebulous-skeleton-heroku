To create and serve up a Nebulous CMS website hosted on `herokuapp.com` in under 3 mins, please continue reading.

Let's go!

## Pre-requisites ##

For this tutorial it is assumed:

1. you already have an account with Heroku
1. you have already created a new project on Heroku called `my-project`
1. you have the `heroku` command line tool installed (see
   [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) if not)
1. you are familiar with both Heroku and Git

## Clone the Nebulous CMS Skeleton ##

Let's create a new website called `my-project`. We'll first get the skeleton code and check it runs locally:

```
$ git clone https://gitlab.com/nebulous-cms/nebulous-skeleton-heroku.git my-project
$ cd my-project
$ npm install
$ npm start
```

Next browse to `http://localhost:3000` and if all of that looks good then set up your Heroku remote and push.

## Pushing to Heroku ##

When pushing to Heroku we need to do two steps. The first is to set up the `heroku` remote and the second actually
pushing the code.

```
$ heroku git:remote -a my-project
set git remote heroku to https://git.heroku.com/my-project.git

$ git push heroku master
...etc...
remote: Verifying deploy... done.
To https://git.heroku.com/my-project.git
 * [new branch]      master -> master
```

Awesome! Congratulations on deploying your first [Nebulous CMS](https://nebulous-cms.org/) website. If you like it so
far, then we'd love it if you could star the [Nebulous CMS](https://gitlab.com/nebulous-cms/nebulous-server) project
on GitLab.

## Change your Content ##

Head into your `content/` dir and create two new files called `about.md` and `about.json`. Type whatever you want into
`about.md` but please add the following (correctly) into `about.json`:

```
{
  "title": "About"
}
```

Restart your local Nebulous server and you should now be able to browse to the `/about` page in your new site.

To see these changes on your live app, commit the code and re-push to the Heroku remote.

## Further Instructions ##

Further information can be found on the main [Nebulous CMS](https://nebulous-cms.org/) website, or continue this
tutorial on the [Nebulous Intro](https://nebulous-cms.org/intro) page.

## Links ##

|               | Website                                     | Twitter                                               |
|:-------------:|:-------------------------------------------:|:-----------------------------------------------------:|
| Project       | [Nebulous CMS](https://nebulous-cms.org/)   | [@NebulousCMS](https://twitter.com/NebulousCMS)       |
| Company       | [Nebulous Design](https://nebulous.design/) | [@NebulousDesign](https://twitter.com/NebulousDesign) |
| Author        | [Andrew Chilton](https://chilts.org/)       | [@andychilton](https://twitter.com/andychilton)       |

## License ##

MIT.

(Ends)
